=== LinkShare ===
Contributors: Bestony
Tags: Links,Share,Posttype
Requires at least: 4.7
Tested up to: 4.8
Stable tag: 0.0.3
License: GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html

use shortcode [linkshare][/linkshare] to show your website share on pages, posts or widgets.

== Description ==
You can use short code [linkshare][/linkshare] to share website on your post,page or widget.
You should add link at admin panel.

== Installation ==
1. Download Plugin Zip File 
2. Upload to Your WordPress
3. create a new page with content : [linkshare][/linkshare]

== Frequently Asked Questions ==
Question 1 : Only Chinese?
Answer: I will support English in next days.

== Screenshots ==
1. https://postimg.aliavv.com/rmbp/fiyar.png
2. https://postimg.aliavv.com/rmbp/si8qf.png
3. https://postimg.aliavv.com/rmbp/ruyn7.png

== Changelog ==
= 0.0.1 =
1. compete short code
2. compete post type
3. compete submit meta box
= 0.0.2 =
1. sanitize, escape, and validate POST calls
= 0.0.3 =
1. use esc_url for input